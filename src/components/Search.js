import React, { Component } from 'react';

class Search extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="Search cols col-lg-5 col-md-5 col-sm-12 col-sx-12">
                <input className={this.props.mode ? "form-control form-dark-mode" : "form-control form-light-mode"} type="text" placeholder="&#x1F50D;  Search for a country ..." onChange={(e) => this.props.onKeywordChange(e)} onBlur={(e) => this.props.onFocusOut(e) } />
            </div>
        )
    }
}

export default Search;