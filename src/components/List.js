import React from 'react';
import Card from './Card';

const List = (props) => {

	return (
		<div className="row ">
			{props.countriesData &&
				props.countriesData.map((el, i) => (
					<Card mode={props.mode} modeText={props.modeText} changeMode={props.changeMode}
						key={i}
						id={el.alpha3Code}
						title={el.name.common}
						flagImg={el.flags.png}
						populationNum={el.population}
						regionIn={el.region}
						capitalCity={el.capital}
					/>
				))}
		</div>
	);
};

export default List;
