import React, { Component } from 'react'

class Select extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="Select cols col-lg-3 col-md-5">
                <select onChange={(e) => this.props.onSelectedRegion(e)} onBlur={(e) => this.props.onFocusOut(e) } id="inputState" className={this.props.mode ? "form-control form-dark-mode" : "form-control form-light-mode"} >
                    <option value="">Filter by region</option>
                    <option value="africa">Africa</option>
                    <option value="americas">Americas</option>
                    <option value="asia">Asia</option>
                    <option value="europe">Europe</option>
                    <option value="oceania">Oceania</option>
                </select>
            </div>
        )
    }
}

export default Select;