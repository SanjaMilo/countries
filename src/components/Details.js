import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// install package: npm i countries-code
import { countriesQuery } from 'countries-code';

class Details extends Component {
	constructor(props) {
		super(props);
		this.state = {
			country: []
		};
	}

	componentDidMount() {
		fetch(`https://restcountries.com/v3.1/name/${this.props.match.params.currentCountry}`)
			.then((res) => res.json())
			.then((data) => {
				console.log(data);
				return this.setState({
					country: data.slice()
				});
			});
	}

	render() {
		const countriesQuery = require('countries-code');
		console.log(countriesQuery.allCountriesList());

		return (
			<React.Fragment>
				 {this.state.country.length > 0 ? 
				
					(<div className="Details">
						<div className="row row-back">
							<div className="cols col-lg-12 col-md-12 col-sm-12 col-sx-12">
								<Link
									className={
										this.props.mode ? 'btn btn-back form-dark-mode' : 'btn btn-back form-light-mode'
									}
									to="/"
								>
									&#x27F5; &nbsp; Back{' '}
								</Link>
							</div>
						</div>
						<div className="row row-flag-text">
							<div className="cols col-lg-5 col-md-12 col-sm-12 col-sx-12">
								<img
									className="img-flag"
									src={this.state.country[0] && this.state.country[0].flags.png}
									alt={this.state.country[0] && this.state.country[0].name.common}
								/>
							</div>
							<div className="cols col-lg-5 col-md-12 col-sm-12 col-sx-12">
								<div className="row">
									<div className="cols col-lg-12 col-md-12 col-sm-12 col-sx-12">
										<h3>{this.state.country[0] && this.state.country[0].name.common}</h3>
									</div>
								</div>
								<div className="row">
									<div className="cols col-lg-6 col-md-12 col-sm-12 col-sx-12 text">
										<p>
											Official Name:{' '}
											<span className="grey">
												{this.state.country[0] && this.state.country[0].name.official}
											</span>
										</p>
										<p>
											Population:{' '}
											<span className="grey">
												{this.state.country[0] &&
													this.state.country[0].population
														.toString()
														.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
											</span>
										</p>
										<p>
											Region:{' '}
											<span className="grey">
												{this.state.country[0] && this.state.country[0].region}
											</span>
										</p>
										<p>
											Sub Region:{' '}
											<span className="grey">
												{this.state.country[0] && this.state.country[0].subregion}
											</span>
										</p>
										<p>
											Capital:{' '}
											<span className="grey">
												{this.state.country[0] && this.state.country[0].capital}
											</span>
										</p>
									</div>
									<div className="cols col-lg-6 col-md-12 col-sm-12 col-xs-12">
										<p>
											Top Level Domain:{' '}
											<span className="grey">
												{this.state.country[0] &&
													this.state.country[0].tld[0]}
											</span>
										</p>
										<p>
											Currencies:{' '}
											<span className="grey">
												{this.state.country[0] &&
													Object.keys(this.state.country[0].currencies)[0]}
											</span>
										</p>
										<p>
											Languages:{' '}
											<span className="grey">
												{this.state.country[0] && 
													Object.keys(this.state.country[0].languages)[0]}
											</span>
										</p>
                                        <p>
											Area:{' '}
											<span className="grey">
												{this.state.country[0] && 
													this.state.country[0].area}
											</span>
										</p>
                                        <p>
											Status:{' '}
											<span className="grey">
												{this.state.country[0] && 
													this.state.country[0].status}
											</span>
										</p>
									</div>
								</div>
								<div className="row">
									<div className="cols col-lg-12 col-md-12 col-sm-12 col-xs-12">
										{/* <p>
											Border Countries:{' '}
											{this.state.country[0] &&
												this.state.country[0].borders.map((el, i) => (
													<span
														className={
															this.props.mode ? (
																'borders grey dark-mode-el'
															) : (
																'borders grey light-mode-borders'
															)
														}
														key={i}
													>
														{' '}
														{countriesQuery.getCountry(el)}{' '}
													</span>
												))}
										</p> */}
									</div>
								</div>
							</div>
						</div>
					</div>) : null
                 }
			</React.Fragment>
		);
	}
}

export default Details;
