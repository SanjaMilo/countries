import React, { Component } from 'react';
import Search from './Search';
import Select from './Select';
import List from './List';

class HomePage extends Component {
    constructor(props) {
        super(props);

        this.allCountries = [];

		this.state = {
            countries: [],
            n: 16
		};
    }

    componentDidMount() {
        // https://restcountries.eu/rest/v2/all // old version, not working 
        //https://restcountries.com/v3.1/all // new version 2022
		fetch('https://restcountries.com/v3.1/all')
		.then((res) => res.json())
		.then((data) => {
            this.allCountries = data.slice();
            
			this.setState({
				countries: this.allCountries.slice(0, 8) // if no load more, than use .slice() for all data
			});
			console.log(data); 
            console.log(this.allCountries); 
        });
	}

    // Function for filtering by keyword entered in input field
    onChange = (e) => {
        let keyword = e.target.value;
        let filteredArr = this.allCountries.filter(el => (el.name.common.toLowerCase().indexOf(keyword.toLowerCase()) > -1 ));
        this.setState({ countries: filteredArr });
    }

    // Function to empty input field off-focus
    onFocusOut = (e) => {
        e.target.value = "";  
    }

    // Function for filtering by selected option field for region
    onSelected = (e) => {
        if (e.target.value === "") {
            this.setState({ countries: this.allCountries })
        } else if (e.target.value === "africa") {
            this.setState({ countries: this.allCountries.filter(el => el.region === 'Africa')})
        } else if (e.target.value === "americas") {
            this.setState({ countries: this.allCountries.filter(el => el.region === 'Americas')})
        } else if (e.target.value === "asia") {
            this.setState({ countries: this.allCountries.filter(el => el.region === 'Asia')})
        } else if (e.target.value === 'europe') {
            this.setState({ countries: this.allCountries.filter(el => el.region === 'Europe')})
        } else if (e.target.value === 'oceania') {
            this.setState({ countries: this.allCountries.filter(el => el.region === 'Oceania')})
        }
    }

    // Function for Load more button, set to load + 8 more cards 
    LoadMore = () => {
        let countriesLoad = this.allCountries.slice(0, this.state.n);
        this.setState({
            countries: countriesLoad,
            n: this.state.n + 8
        });
    }

    render() {
        return (
            <div className="HomePage container-fluid">
                <div className="row search-select">
                    <Search onKeywordChange={this.onChange} onFocusOut={this.onFocusOut} mode={this.props.mode} modeText={this.props.modeText} changeMode={this.props.changeMode} />
                    <Select onSelectedRegion={this.onSelected} onFocusOut={this.onFocusOut} mode={this.props.mode} modeText={this.props.modeText} changeMode={this.props.changeMode} />
                </div>
                <div className="Homepage-row">
                    <List countriesData={this.state.countries} mode={this.props.mode} modeText={this.props.modeText} changeMode={this.props.changeMode} />
                </div>
                <div className="row row-load-more">
                    <div className="col-lg-12 col-md-12 col-sm-12 col-sx-12 d-flex justify-content-center">
                        <button className={this.props.mode ? "btn btn-load-more form-dark-mode" : "btn btn-load-more form-light-mode"} onClick={this.LoadMore} >Load more</button> 
                    </div>
                </div>
               
            </div>
        )
    }
}

export default HomePage;