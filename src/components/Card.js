import React from 'react';
import {Link} from 'react-router-dom';

const Card = (props) => {
	return (
		<div className="Card cols col-lg-3 col-md-6 col-sm-12 col-xs-12">
            <Link to={"/details/" + props.title.toLowerCase()}>
            <div className={props.mode ? "card h-100 dark-mode-el" : "card h-100 light-mode-el"}>
				<img src={props.flagImg} className="card-img-top" alt={props.title} />
				<div className="card-body">
					<h5 className="card-title">{props.title}</h5>
					<p className="card-text">Population: <span className="grey">{props.populationNum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span></p>	
					<p className="card-text">Region: <span className="grey">{props.regionIn}</span></p>	
                    <p className="card-text">Capital: <span className="grey">{props.capitalCity}</span></p>
				</div>
			</div>
            </Link>	
		</div>
	);
};

export default Card;
