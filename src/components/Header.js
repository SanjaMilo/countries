import React, { Component } from 'react';

class Header extends Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            <nav className={this.props.mode ? "Header dark-mode-el" : "Header light-mode-el"}>
                <h2>Where in the world?</h2>
                <button className={this.props.mode ? "btn dark-mode-el" : "btn light-mode-el"} onClick={this.props.changeMode}>
                <i className={(this.props.mode ? "far fa-moon": "fas fa-moon")}></i>
                    {this.props.modeText}
                </button>
            </nav>
        )
    }
}

export default Header;

