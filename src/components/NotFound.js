import React from 'react';
import {Link} from 'react-router-dom';

const NotFound = () => {
    return (
        <div className="NotFound">
            <h1>Page Not Found</h1>
            <Link to="/" ><button className="btn dark-mode-el">&#x27F5;  Go Back To Home Page</button></Link>
        </div>
    );
}

export default NotFound;
