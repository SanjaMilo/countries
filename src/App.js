import React from 'react';
// import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import Header from './components/Header';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import HomePage from './components/HomePage';
import Details from './components/Details';
import NotFound from './components/NotFound';

class App extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
            inner: true,
            innerText: 'Light Mode'
        }
	}

	changeMode = () => {
        if (!this.state.inner) {
            this.setState({
                inner: true,
                innerText: "Light Mode"
            });
        } else {
            this.setState({
                inner: false,
                innerText: "Dark Mode"
            });
        }
    }

	render() {
		return (
			<Router>
				<div className={this.state.inner ? "App dark-mode" : "App light-mode"}>
					<Header mode={this.state.inner} modeText={this.state.innerText} changeMode={this.changeMode} />
					<Switch>
						<Route exact path="/" render={() => <HomePage mode={this.state.inner} changeMode={this.changeMode} />} />
						<Route path="/details/:currentCountry" render={(props) => <Details mode={this.state.inner}  changeMode={this.changeMode} {...props} /> } />
						<Route component={NotFound} />
					</Switch>
				</div>
			</Router>
		);
	}
}

export default App;
